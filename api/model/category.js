'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name: {
        type: String,
        require: true
    },

    modified: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Update modified datetime
 */
CategorySchema.pre('save', function(next){
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model('Category', CategorySchema);