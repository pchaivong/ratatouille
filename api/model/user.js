'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;

const UserSchema = new Schema({
    username: {
        type: String,
        require: true,
        unique: true
    },

    password: {
        type: String,
    },

    email: String
});

UserSchema.pre('save', function(next){
    let user = this;
    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_WORK_FACTOR)
    .then((salt) => {
        bcrypt.hash(user.password, salt, null)
        .then((hash) => {
            user.password = hash;
            next();
        })
        .catch((err) => {
            console.error(err);
            return next(err);
        });
    })
    .catch((err)=> {
        console.error(err);
        return next(err);
    });
});

UserSchema.method.validatePassword = function(candidatedPassword, cb){
    bcrypt.compare(candidatedPassword, this.password)
    .then((isMatched) => {
        cb(null, isMatched);
    })
    .catch((err) => {
        return cb(err);
    });
}

module.exports = mongoose.model('User', UserSchema);