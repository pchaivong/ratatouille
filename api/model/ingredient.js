'use strict';

const mongoose = require('mongoose');
const schema = mongoose.Schema;

var IngredientSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: String,
    tags: [String],
    restaurant: {
        type: Schema.Types.ObjectId,
        ref: 'Restaurant'
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    unit: String,
    pricePerUnit: {
        type: Number,
        default: 0.0
    },
    unitSize: {
        type: Number,
        default: 0.0
    }, // For deduct from stock
    inStock: Number, // Amount of this ingredient left in stock

    // TODO: Add cost


    modified: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date,
        default: Date.now
    }
});


/**
 * Update modified datetime
 */
IngredientSchema.pre('save', function(next){
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model('Ingredient', IngredientSchema);