'use strict';

const mongoose = require('mongoose')
const Schema = mongoose.Schema;

var RestaurantSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: String,
    address: String,
    telephone: String,
    owner: {type: Schema.Types.ObjectId, ref: 'User'},
    created: {type: Date, default: Date.now},
    modified: {type: Date, default: Date.now}
});

/**
 * Update modified date time
 */
RestaurantSchema.pre('save', function(next){
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model('Restaurant', RestaurantSchema);