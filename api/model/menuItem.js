'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var MenuItemSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: String,
    categories: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Category'
        }
    ],
    ingredients: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Ingredient'
        }
    ],
    price: {
        type: Number,
        default: 0.0
    },
    disable: {
        type: boolean,
        default: false
    },

    modified: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Update modified datetime
 */
MenuItemSchema.pre('save', function(next){
    this.modified = Date.now();
    next();
});

module.exports = mongoose.model('MenuItem', MenuItemSchema);