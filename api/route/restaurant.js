'use strict';

const Handler = require('../controller/restaurant');
const Joi = require('joi');
const routeTags = ['api', 'restaurant'];

module.exports = [
    {
        method: 'GET',
        path: '/api/restaurant',
        config: {
            tags: routeTags,
            description: 'Get Restaurant list',
            notes: 'REST API for getting the list of restaurants',
            handler: Handler.getRestaurantList
        }
    },

    {
        method: 'GET',
        path: '/api/restaurant/{id}',
        config: {
            tags: routeTags,
            description: 'Get restaurant detail',
            notes: 'REST API for getting the detail of specified restaurant',
            handler: Handler.getRestaurantDetail,
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/restaurant',
        config: {
            tags: routeTags,
            description: 'Create restaurant',
            notes: 'REST API for creating new restaurant',
            handler: Handler.createRestaurant,
            validate: {
                payload: {
                    name: Joi.string().required(),
                    description: Joi.string(),
                    address: Joi.string(),
                    telephone: Joi.string(),
                    owner: Joi.string()
                }
            }
        }
    },

    {
        method: 'DELETE',
        path: '/api/restaurant/{id}',
        config: {
            tags: routeTags,
            description: 'Delete restaurant',
            notes: 'REST API for deleting restaurant',
            handler: Handler.deleteRestaurant,
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },

    {
        method: 'PUT',
        path: '/api/restaurant/{id}',
        config: {
            tags: routeTags,
            description: 'Update restaurant',
            notes: 'REST API for updating restaurant detail',
            handler: Handler.updateRestaurant,
            validate: {
                params: {
                    id: Joi.string().required()
                },
                payload: {
                    name: Joi.string(),
                    description: Joi.string(),
                    address: Joi.string(),
                    telephone: Joi.string(),
                    owner: Joi.string()
                }
            }
        }
    }
]