'use strict';

const Restarant = require('../model/restaurant');
const Boom = require('boom');

exports.createRestaurant = function(request, reply){
    var restaurant = request.payload;

    Restarant.create(restaurant)
    .then((result) => {
        console.log('Restaurant is created');
        console.log(result);
        return reply(result).code(201); // Created
    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err));
    });
}

exports.deleteRestaurant = function(request, reply){
    var restaurantId = request.params.id;

    Restarant.findOneAndDelete({_id: restaurantId})
    .then((result) => {
        if (!result){
            console.log(`Restaurant(${restaurantId}) is not found`);
            return reply(Boom.notFound);
        }
        console.log(`Restaurant (id: ${restaurantId}) is deleted`);
        return reply().code(204);
    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err));
    });
}

exports.updateRestaurant = function(request, reply){
    var restaurantId = request.params.id;
    var restaurant = request.payload;

    Restarant.findOneAndUpdate({_id: restaurantId}, restaurant)
    .then((result) => {
        if (!result){
            console.log(`Restaurant(${restaurantId}) is not found`);
            return reply(Boom.notFound);
        }
        console.log(`Restaurant id: ${restaurantId} is updated`);
        return reply(result).code(201);
    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err))
    })
}

exports.getRestaurantDetail = function(request, reply){
    var restaurantId = request.params.id;

    Restarant.findById(restaurantId)
    .then((result) => {
        if (!result){
            console.log(`Restaurant(${restaurantId}) is not found`);
            return reply(Boom.notFound);
        }
        console.log(`Restaurant(${restaurantId}) is retrieved`);
        return reply(result).code(200);
    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err));
    });
}

exports.getRestaurantList = function(request, reply){
    var resList = []

    Restarant.find({})
    .then((results) => {
        if(results){
            results.forEach((item) => {
                resList.push(item);
            });
        }
        return reply(resList).code(200);
    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err));
    });
}