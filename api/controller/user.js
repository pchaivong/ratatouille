'use strict';

const JWT = require('jsonwebtoken');
const User = require('../model/user');
const Boom = require('boom');

exports.createUser = function(request, reply){
    let user = request.payload;
    User.create(user)
    .then((newUser) => {
        reply(newUser).code(201);
    })
    .catch((err) => {
        console.error(err);
        reply(Boom.badImplementation(err.message));
    });
}

exports.login = function(request, reply){
    let credential = request.payload;

    User.findOne({username: credential.username})
    .then((user) => {
        if (!user){
            console.log(`Incorrect username or password`);
            return reply(Boom.unauthorized('Incorrect username or password'));
        }
        // Verify password
        user.validatePassword(credential.password, function(err, isMatch){
            if (err){
                console.error(err);
                return reply(Boom.badImplementation(err.message));
            }
            if (!isMatch){
                console.log('Incorrect username or password');
                return reply(Boom.unauthorized('Incorrect username or password'));
            }
            let obj = user.toObject();
            delete obj.password;
            let signedToken = {
                token: JWT.sign(obj, 'SOMESECRET')
            }
            return reply(signedToken).code(200);
        });

    })
    .catch((err) => {
        console.error(err);
        return reply(Boom.badImplementation(err.message));
    });
}