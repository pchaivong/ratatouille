
const pkg = require('./package.json');
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Mongoose = require('mongoose');

var routes = require('./api/route');


Mongoose.connect('mongodb://localhost/ratatouille', {server: { poolSize: 3}});

let server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 4444,
    routes: {cors: true}
});

let swaggerOptions = {
    info: {
        'title': 'Ratatouille API documentation',
        'version': pkg.version
    }
};

server.register([
    Inert,
    Vision,
    {
        register: HapiSwagger,
        options: swaggerOptions
    },
], function(err){
    if (err){
        console.error(err);
        throw err;
    }
    server.start(() => {
        server.route(routes);
        console.log('info', 'Server is running at: ' + server.info.uri);
    });
});